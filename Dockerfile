FROM golang:1-alpine AS build

RUN apk --no-cache add git ca-certificates

COPY . /data
WORKDIR /data
RUN CGO_ENABLED=0 go build -ldflags '-s -w' -o /tmp/dns-update .


FROM alpine

RUN apk add --no-cache jq grep ca-certificates
COPY --from=build /tmp/dns-update /usr/local/bin/dns-update
CMD ["/usr/local/bin/dns-update"]
