package main

import (
	"bytes"
	"crypto/tls"
	"github.com/rs/zerolog"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"time"

	"codeberg.org/momar/dns-update/dns"
	"codeberg.org/momar/dns-update/dns/providers"
	"github.com/rs/zerolog/log"
)

// https://stackoverflow.com/a/106223
var validHostnameRegex = regexp.MustCompile(`^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$`)
var validIP4 = regexp.MustCompile(`^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$`)
var validIP6 = regexp.MustCompile(`^(?:[0-9a-f]{0,4}:){2,7}[0-9a-f]{0,4}$`)

func main() {
	// Read & check environment variables

	fail := false

	disable4 := os.Getenv("DISABLE4") != "" && os.Getenv("DISABLE4") != "false" && os.Getenv("DISABLE4") != "no" && os.Getenv("DISABLE4") != "0"
	disable6 := os.Getenv("DISABLE6") != "" && os.Getenv("DISABLE6") != "false" && os.Getenv("DISABLE6") != "no" && os.Getenv("DISABLE6") != "0"
	debug := os.Getenv("DEBUG") != "" && os.Getenv("DEBUG") != "false" && os.Getenv("DEBUG") != "no" && os.Getenv("DEBUG") != "0"
	setupLog(debug)

	domain := os.Getenv("DOMAIN")
	if domain == "" || !validHostnameRegex.MatchString(domain) {
		log.Error().Msg("DOMAIN is required and has to be a valid hostname")
		fail = true
	}

	provider, err := providers.Get(os.Getenv("PROVIDER"))
	if os.Getenv("PROVIDER") == "" {
		log.Error().Msg("PROVIDER is required")
		fail = true
	} else if err != nil {
		log.Error().Err(err).Msg("provider initialization failed")
		fail = true
	}

	interval, err := time.ParseDuration(os.Getenv("INTERVAL"))
	if err != nil {
		log.Info().Msg("INTERVAL not set, using default (15m)")
		interval = 15 * time.Minute
	}

	check4 := os.Getenv("CHECK4")
	if check4 == "" {
		check4 = "https://api.ipify.org"
	}
	check4trust := os.Getenv("CHECK4_TRUST") != "" && os.Getenv("CHECK4_TRUST") != "false" && os.Getenv("CHECK4_TRUST") != "no" && os.Getenv("CHECK4_TRUST") != "0"
	check4transformations := []Transformation{}
	if expr := os.Getenv("CHECK4_JQ"); expr != "" {
		check4transformations = append(check4transformations, TransformJQ(expr))
	}
	if expr := os.Getenv("CHECK4_GREP"); expr != "" {
		check4transformations = append(check4transformations, TransformGREP(expr))
	}

	check6 := os.Getenv("CHECK6")
	if check6 == "" {
		check6 = "https://api6.ipify.org"
	}
	check6trust := os.Getenv("CHECK6_TRUST") != "" && os.Getenv("CHECK6_TRUST") != "false" && os.Getenv("CHECK6_TRUST") != "no" && os.Getenv("CHECK6_TRUST") != "0"
	check6transformations := []Transformation{}
	if expr := os.Getenv("CHECK6_JQ"); expr != "" {
		check6transformations = append(check6transformations, TransformJQ(expr))
	}
	if expr := os.Getenv("CHECK6_GREP"); expr != "" {
		check6transformations = append(check6transformations, TransformGREP(expr))
	}

	if fail {
		os.Exit(1)
	}

	// Endless loop

	first := true
	for {
		if !first {
			time.Sleep(interval)
		}

		// Get current IP addresses of this machine

		var ip4, ip6 string
		if !disable4 {
			ip4 = getIP("IPv4", check4, validIP4, check4trust, check4transformations...)
		}
		if !disable6 {
			ip6 = getIP("IPv6", check6, validIP6, check6trust, check6transformations...)
		}

		// Update records

		for recordType, ip := range map[string]string{"A": ip4, "AAAA": ip6} {
			if ip == "" {
				continue
			}

			recs, err := provider.Get(recordType, domain+".")
			if err != nil {
				log.Error().Err(err).Msgf("%s record retrieval failed", recordType)
				continue
			}
			if len(recs) == 0 {
				err := provider.Create(dns.Record{
					Type:    recordType,
					Name:    domain + ".",
					Content: ip,
					TTL:     interval,
				})
				if err != nil {
					log.Error().Err(err).Msgf("%s record creation failed", recordType)
					continue
				}
				log.Info().Str("domain", domain).Str("ip", ip).Msgf("%s record has been created", recordType)
			} else if recs[0].Content != ip {
				old := recs[0].Content
				recs[0].Content = ip
				recs[0].TTL = interval
				err := provider.Update(dns.Record{Type: recs[0].Type, Name: recs[0].Name, Content: old}, recs[0])
				if err != nil {
					log.Error().Err(err).Msgf("%s record update failed", recordType)
					continue
				}
				log.Info().Str("domain", domain).Str("ip", ip).Str("old-ip", old).Msgf("%s record has been updated", recordType)
			} else if first {
				log.Info().Str("domain", domain).Str("old-ip", recs[0].Content).Msgf("No %s record change required, keeping old value (this message is only shown once - set DEBUG=1 to show every check)", recordType)
			} else {
				log.Debug().Str("domain", domain).Str("old-ip", recs[0].Content).Msgf("No %s record change required, keeping old value", recordType)
			}
		}
		first = false
	}
}

var ip6error bool // make sure to only print the warning about a missing IPv6 once
var c = http.Client{Timeout: 30 * time.Second}

func getIP(desc string, url string, validator *regexp.Regexp, trust bool, transformations ...Transformation) string {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: trust}
	res, err := c.Get(url)
	if err != nil {
		log.Error().Err(err).Msgf("couldn't determine %s address (during request)", desc)
		return ""
	}
	ip, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Error().Str("status", res.Status).Err(err).Msgf("couldn't determine %s address (during body read)", desc)
		return ""
	}
	ip = bytes.TrimSpace(ip)
	for _, transformation := range transformations {
		ip, err = transformation.Transform(ip)
		if err != nil {
			log.Error().Err(err).Msgf("couldn't determine %s address (transformation %s failed)", desc, transformation.Name())
			return ""
		}
	}
	ip = bytes.TrimSpace(ip)
	if res.StatusCode != 200 || !validator.Match(ip) { // we didn't get a valid IP
		if desc == "IPv6" && validIP4.Match(ip) { // we want an IPv6 IP, but got an IPv4 IP
			if !ip6error {
				log.Warn().Msg("IPv6 address seems not to exist (got IPv4) - disabling IPv6 updater until a valid address can be found")
			}
			ip6error = true
		} else {
			log.Error().Str("status", res.Status).Bytes("body", ip).Msgf("Couldn't determine %s address", desc)
		}
		return ""
	}
	if desc == "IPv6" && ip6error { // we got an IPv6 IP after it was broken before
		log.Warn().Msg("found an IPv6 address - enabling IPv6 updater again")
		ip6error = false
	}
	return string(ip)
}

func setupLog(debug bool) {
	log.Logger = zerolog.New(zerolog.NewConsoleWriter(func(w *zerolog.ConsoleWriter) {
		w.TimeFormat = "2006-01-02 15:04:05"
	})).Hook(zerolog.HookFunc(func(e *zerolog.Event, level zerolog.Level, message string) {
		*e = *(e.Timestamp())
	}))
	if debug {
		log.Logger = log.Logger.Level(zerolog.TraceLevel)
	} else {
		log.Logger = log.Logger.Level(zerolog.InfoLevel)
	}
}
