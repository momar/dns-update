package providers

import (
	"errors"
	"os"
	"strings"

	"codeberg.org/momar/dns-update/dns"
	"codeberg.org/momar/dns-update/dns/providers/hostingde"
)

type Option struct {
	Name  string
	Value string
}

func Get(name string, options ...Option) (dns.Provider, error) {
	opt := map[string]string{}
	for _, env := range os.Environ() {
		envSplit := strings.SplitN(env, "=", 2)
		opt[envSplit[0]] = envSplit[1]
	}

	switch name {
	case "hostingde":
		return hostingde.New(opt)
	}
	return nil, errors.New("no provider found with this name")
}
