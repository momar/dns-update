package hostingde

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"codeberg.org/momar/dns-update/dns"
)

const endpoint = "https://secure.hosting.de/api"

var client = http.Client{Timeout: 30 * time.Second}

type Provider map[string]string

func New(options map[string]string) (Provider, error) {
	if options["HOSTINGDE_API_KEY"] == "" {
		return nil, errors.New("HOSTINGDE_API_KEY is required")
	}
	return Provider(options), nil
}

func (options Provider) Get(recordType string, name string) ([]dns.Record, error) {
	zone, err := options.zoneByName(name)
	if err != nil {
		return nil, err
	}

	result := []dns.Record{}
	for _, rec := range zone.Records {
		if rec.RecordType == recordType && rec.Name == strings.TrimSuffix(name, ".") {
			result = append(result, dns.Record{
				Type:    rec.RecordType,
				Name:    rec.Name + ".",
				Content: rec.Content,
				TTL:     time.Duration(rec.TTL) * time.Second,
			})
		}
	}

	return result, nil
}

func (options Provider) Create(record dns.Record) error {
	zone, err := options.zoneByName(record.Name)
	if err != nil {
		return err
	}

	return options.zoneUpdate(zone, []interface{}{
		map[string]interface{}{
			"name":    strings.TrimSuffix(record.Name, "."),
			"type":    record.Type,
			"content": record.Content,
			"ttl":     int(record.TTL.Seconds()),
		},
	}, []interface{}{})
}

func (options Provider) Update(record dns.Record, newRecord dns.Record) error {
	zone, err := options.zoneByName(record.Name)
	if err != nil {
		return err
	}

	// Replace the record
	return options.zoneUpdate(zone, []interface{}{
		map[string]interface{}{
			"name":    strings.TrimSuffix(newRecord.Name, "."),
			"type":    newRecord.Type,
			"content": newRecord.Content,
			"ttl":     int(newRecord.TTL.Seconds()),
		},
	}, []interface{}{
		map[string]interface{}{
			"name":    strings.TrimSuffix(record.Name, "."),
			"type":    record.Type,
			"content": record.Content,
		},
	})
}

func (options Provider) Delete(record dns.Record) error {
	zone, err := options.zoneByName(record.Name)
	if err != nil {
		return err
	}

	return options.zoneUpdate(zone, []interface{}{}, []interface{}{
		map[string]interface{}{
			"name":    strings.TrimSuffix(record.Name, "."),
			"type":    record.Type,
			"content": record.Content,
			"ttl":     int(record.TTL.Seconds()),
		},
	})
}

type zoneConfig struct {
	ID                    string   `json:"id"`
	NameACE               string   `json:"name"`
	Name                  string   `json:"nameUnicode"`
	MasterID              string   `json:"masterId"`
	Type                  string   `json:"type"`
	EmailAddress          string   `json:"emailAddress"`
	DNSSecMode            string   `json:"dnsSecMode"`
	ZoneTransferWhitelist []string `json:"zoneTransferWhitelist"`
	SoaValues             struct {
		Refresh     int `json:"refresh"`
		Retry       int `json:"retry"`
		Expire      int `json:"expire"`
		TTL         int `json:"ttl"`
		NegativeTTL int `json:"negativeTtl"`
	} `json:"soaValues"`
	TemplateValues struct {
		TemplateID           string `json:"templateId"`
		TieToTemplate        bool   `json:"tieToTemplate"`
		TemplateReplacements struct {
			IPv4Replacement     string `json:"ipv4Replacement"`
			IPv6Replacement     string `json:"ipv6Replacement"`
			MailIPv4Replacement string `json:"mailIpv4Replacement"`
			MailIPv6Replacement string `json:"mailIpv6Replacement"`
		} `json:"templateReplacements"`
	}
}

type zone struct {
	ZoneConfig zoneConfig `json:"zoneConfig"`
	Records    []struct {
		RecordID   string `json:"id"`
		ZoneID     string `json:"zoneId"`
		RecordType string `json:"type"`
		Name       string `json:"name"`
		Content    string `json:"content"`
		TTL        int    `json:"ttl"`
	} `json:"records"`
}

func (options Provider) zoneByName(name string) (zone, error) {
	nameSplit := strings.Split(name, ".")
	zoneStart := -1
	var z zone
	var lastError error
	for i := 0; zoneStart < 0 && i < len(nameSplit); i++ {
		payload := map[string]interface{}{
			"authToken": options["HOSTINGDE_API_KEY"],
			"filter": map[string]interface{}{
				"field": "zoneNameUnicode",
				"value": strings.TrimSuffix(strings.Join(nameSplit[i:], "."), "."),
			},
		}
		zones := struct {
			Response struct {
				Data         []zone `json:"data"`
				TotalEntries int    `json:"totalEntries"`
			} `json:"response"`
		}{}

		var err error
		var req, body []byte
		var res *http.Response
		req, err = json.Marshal(payload)
		if err == nil {
			res, err = client.Post(endpoint+"/dns/v1/json/zonesFind", "application/json", bytes.NewReader(req))
		}
		if err == nil {
			body, err = ioutil.ReadAll(res.Body)
		}
		if err == nil {
			err = json.Unmarshal(body, &zones)
		}

		if err != nil {
			lastError = err
		} else if res.StatusCode != 200 {
			lastError = errors.New(res.Status + ": " + string(body))
		} else if zones.Response.TotalEntries > 0 {
			zoneStart = i
			z = zones.Response.Data[0]
			break
		}
	}
	if zoneStart < 0 && lastError != nil {
		return z, errors.New(lastError.Error())
	} else if zoneStart < 0 {
		return z, errors.New("no matching zone found")
	}
	return z, nil
}

func (options Provider) zoneUpdate(zone zone, recordsToAdd []interface{}, recordsToDelete []interface{}) error {
	payload := map[string]interface{}{
		"authToken":       options["HOSTINGDE_API_KEY"],
		"zoneConfig":      zone.ZoneConfig,
		"recordsToAdd":    recordsToAdd,
		"recordsToDelete": recordsToDelete,
	}
	status := struct {
		Status string `json:"status"`
		Errors []struct {
			Text string `json:"text"`
		} `json:"errors"`
		Warnings []struct {
			Text string `json:"text"`
		} `json:"warnings"`
	}{}

	var err error
	var req, body []byte
	var res *http.Response
	req, err = json.Marshal(payload)
	if err == nil {
		res, err = client.Post(endpoint+"/dns/v1/json/zoneUpdate", "application/json", bytes.NewReader(req))
	}
	if err == nil {
		body, err = ioutil.ReadAll(res.Body)
	}
	if err == nil {
		err = json.Unmarshal(body, &status)
	}

	if err != nil {
		return err
	} else if res.StatusCode != 200 {
		return errors.New(res.Status + ": " + string(body))
	} else if len(status.Errors) != 0 {
		return fmt.Errorf("%v", status.Errors)
	}
	time.Sleep(500 * time.Millisecond) // hosting.de takes some time before the next change is allowed
	return nil
}
