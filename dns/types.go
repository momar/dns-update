package dns

import "time"

type Provider interface {
	Get(recordType string, name string) ([]Record, error)
	Create(Record) error
	Update(Record, Record) error
	Delete(Record) error
}

type Record struct {
	Type    string
	Name    string
	Content string
	TTL     time.Duration
}
