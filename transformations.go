package main

import (
	"os/exec"
	"time"
)

type Transformation interface {
	Name() string
	Transform([]byte) ([]byte, error)
}

// [jq -r] - https://stedolan.github.io/jq/manual/

type TransformJQ string

func (TransformJQ) Name() string {
	return "jq"
}

func (expression TransformJQ) Transform(input []byte) ([]byte, error) {
	return pipe(input, "jq", "-r", string(expression))
}

// [grep -Po] https://www.man7.org/linux/man-pages/man1/grep.1.html

type TransformGREP string

func (TransformGREP) Name() string {
	return "grep"
}

func (expression TransformGREP) Transform(input []byte) ([]byte, error) {
	return pipe(input, "grep", "-Po", string(expression))
}

// helper to pipe a byte slice to an external process with a 5 second timeout

func pipe(input []byte, cmd string, args ...string) ([]byte, error) {
	c := exec.Command(cmd, args...)

	stdin, err := c.StdinPipe()
	if err != nil {
		return nil, err
	}
	go func() {
		defer stdin.Close()
		_, err := stdin.Write(input)
		if err != nil {
			return
		}
	}()

	go func() {
		time.Sleep(5 * time.Second)
		if c.ProcessState != nil && c.Process != nil && !c.ProcessState.Exited() {
			c.Process.Kill()
		}
	}()

	return c.Output()
}
