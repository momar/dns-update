# DNS Update

Flexible updater for dynamic DNS configuration.

Just create a `docker-compose.yml`, run `docker-compose up -d`, and you're good to go!

```yaml
version: "3"
services:
  update:
    image: momar/dns-update
    restart: always
    environment:
      DOMAIN: example.org
      INTERVAL: 5m # specifies check interval & TTL (default: 15m)
      PROVIDER: hostingde
      HOSTINGDE_API_KEY: ...
      DISABLE6: true # setting DISABLE[4/6] disables the respective IPv# version
      CHECK4: https://checkip.amazonaws.com # CHECK[4/6] specifies the endpoint to request the current IP from - the defaults are https://api6.ipify.org/ and https://api.ipify.org/
      #CHECK4_TRUST: "yes" # only set this if the server at the check URL doesn't have a valid SSL certificate
      #CHECK4_GREP: 'ip=\K[\d\.]+' # transform the check response using regular expressions with "grep -Po" 
      #CHECK4_JQ: '.ip' # transform the check response JSON with "jq -r" (see https://stedolan.github.io/jq/manual/)
```

## Currently supported providers

- `hostingde` - hosting.de, required options: `HOSTINGDE_API_KEY`

If you want to add a new provider, feel free to create an issue or a pull request. Name and environment variable should be analogical to the [lego DNS providers](https://go-acme.github.io/lego/dns/).
